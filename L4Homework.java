import com.whitev.programming.*;

public class L4Homework{
	public static void main(String[] args){
		StringController sc = new StringController();
		
		sc.reverse("This is a text.");	// 應當輸出 ".txet a si sihT"
		sc.reverse("The quick brown fox jumps over the lazy dog."); // 應當輸出 ".god yzal eht revo spmuj xof nworb kciuq ehT"
		
		System.out.println(sc,getLengthList("This is a text")); // 應當輸出 [4, 2, 1, 4]
		System.out.println(sc,getLengthList("The quick brown fox jumps over the lazy dog")); // 應當輸出 [3, 5, 5, 3, 5, 4, 3, 4, 3]
		
	}
}